/// <reference types="cypress" />


import Cadastro from '../support/pages/Cadastro';
import Compra from '../support/pages/Compra';
import Home from '../support/pages/Home';
import Login from '../support/pages/Login';
import Logout from '../support/pages/Logout';
const NavegadorUrl = Cypress.config("baseUrl")

describe('MyStore', () => {
    it('deve realizar Cadastro', () => {
        cy.visit(NavegadorUrl)
        Home.clicabotãoSignIn();
        Cadastro.preencheremailaddress();
        Cadastro.clicarbotãoCriarConta();
        Cadastro.validarAcessoaTelaDeCadastro();
        Cadastro.selecionarTitle();
        Cadastro.preencherFirstName();
        Cadastro.preencherLastName();
        Cadastro.preencherPassword();
        Cadastro.selecionarDia();
        Cadastro.selecionarMes();
        Cadastro.selecionarAno();
        Cadastro.clicarAssinatura();
        Cadastro.clicarReceberOfertas();
        Cadastro.preencherCompany();
        Cadastro.preencherEndereço();
        Cadastro.preencherLugarEndereço();
        Cadastro.preencherCity();
        Cadastro.selecionarStates();
        Cadastro.preencherCep();
        Cadastro.preencherInformaçõesAdicionais();
        Cadastro.preencherTelefone();
        Cadastro.preencherTelefoneCelular();
        Cadastro.preencheremailalternativo();
        Cadastro.clicarBotãoRegistro();
        Cadastro.validarmensagemCadastroComSucesso();
    })


    it('deve reaalizar Login', () => {
        cy.visit(NavegadorUrl);
        Home.clicabotãoSignIn();
        Login.preencherEmail();
        Login.preencherSenha();
        Login.clicarBotãoLogin();
        Login.validarLogin();
    })

    it('deve realizar Logout', () => {
        cy.visit(NavegadorUrl);
        Home.clicabotãoSignIn();
        Login.preencherEmail();
        Login.preencherSenha();
        Login.clicarBotãoLogin();
        Logout.clicarButtonLogout();
        Logout.validarLogout();
    
    })

    it('deve adicionar um produto no carrinho', () => {
        cy.visit(NavegadorUrl);
        Home.clicabotãoSignIn();
        Login.preencherEmail();
        Login.preencherSenha();
        Login.clicarBotãoLogin();
        Home.clicarseçãomulheres();
        Compra.adicionarPrintedDressnocarrinho();
        Compra.validarprodutoadicionadocomsucesso();
    })

    it('deve adicionar mais de um produto no carrinho', () => {
        cy.visit(NavegadorUrl);
        Home.clicabotãoSignIn();
        Login.preencherEmail();
        Login.preencherSenha();
        Login.clicarBotãoLogin();
        Home.clicarseçãomulheres();
        Compra.adicionarPrintedDressnocarrinho();
        Compra.validarprodutoadicionadocomsucesso();
        Compra.clicarnobotãodecontinuarnoshopping();
        Compra.adicionarBlousenocarrinho(); 
        Compra.validarprodutoadicionadocomsucesso();
    })

    it('deve finalizar compra com cartão de crédito', () => {
        cy.visit(NavegadorUrl);
        Home.clicabotãoSignIn();
        Login.preencherEmail();
        Login.preencherSenha();
        Login.clicarBotãoLogin();
        Home.clicarseçãomulheres();
        Compra.adicionarPrintedDressnocarrinho();
        Compra.validarprodutoadicionadocomsucesso();
        Compra.clicarnobotãodecontinuarnoshopping();
        Compra.adicionarBlousenocarrinho(); 
        Compra.validarprodutoadicionadocomsucesso();
        Compra.clicarnobotãoProceedtoCheckoutNoModal();
        Compra.clicarnobotãoProceedtoCheckoutNoCarrinho();
        Compra.adicionarNovoEndereço();
        Compra.clicarnobotãoProceedtoCheckoutNoAddress();
        Compra.clicarNoCheckAceitarTermos();
        Compra.clicarnobotãoProceedtoCheckoutNoShipping();
        Compra.selecionarCartãoComoFormaDePagamento();
        Compra.clicarNoBotãoDeConfirmarOrdem();
        Compra.clicarnoBotãoDeVerificarOrdens();
        Compra.verificarOrdem();
        Compra.verificarDetalhesDaOrdem();
        Compra.downloadpdf();
    })

    it('deve finalizar compra selecionando cheque', () => {
        cy.visit(NavegadorUrl);
        Home.clicabotãoSignIn();
        Login.preencherEmail();
        Login.preencherSenha();
        Login.clicarBotãoLogin();
        Home.clicarseçãomulheres();
        Compra.adicionarPrintedDressnocarrinho();
        Compra.validarprodutoadicionadocomsucesso();
        Compra.clicarnobotãodecontinuarnoshopping();
        Compra.adicionarBlousenocarrinho(); 
        Compra.validarprodutoadicionadocomsucesso();
        Compra.clicarnobotãoProceedtoCheckoutNoModal();
        Compra.clicarnobotãoProceedtoCheckoutNoCarrinho();
        Compra.adicionarNovoEndereço();
        Compra.clicarnobotãoProceedtoCheckoutNoAddress();
        Compra.clicarNoCheckAceitarTermos();
        Compra.clicarnobotãoProceedtoCheckoutNoShipping();
        Compra.selecionarChequeComoFormaDePagamento();
        Compra.clicarNoBotãoDeConfirmarOrdem();
        Compra.clicarnoBotãoDeVerificarOrdens();
        Compra.verificarOrdem();
        Compra.verificarDetalhesDaOrdem();
        Compra.downloadpdf();
    })

})