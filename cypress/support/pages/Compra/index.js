const element = require('./elements').ELEMENTS;

class Compra{
    adicionarPrintedDressnocarrinho(){
        cy.route('POST', '**/index.php?**').as('postAddToCart');
        cy.get(element.addPrintedDressnocarrinho).click();
        cy.wait('@postAddToCart').then((xhr) => {
            expect(xhr.status).be.eq(200);
           
    })
}
    validarprodutoadicionadocomsucesso(){
        cy.get(element.productaddsucessfully).contains('Product successfully added to your shopping cart');
        cy.wait(500);
        cy.screenshot();
    }
    
    clicarnobotãodecontinuarnoshopping(){
        cy.get(element.buttoncontinueshopping).click();
    }

    adicionarBlousenocarrinho(){
        cy.route('POST', '**/index.php?**').as('postAddToCart');
        cy.get(element.addBlousenocarrinho).click();
        cy.wait('@postAddToCart').then((xhr) => {
            expect(xhr.status).be.eq(200);
    })
    }

    clicarnobotãoProceedtoCheckoutNoModal(){
        cy.get(element.buttonProceedCheckoutnoModal).click();
        cy.screenshot();
    }

    clicarnobotãoProceedtoCheckoutNoCarrinho(){
        cy.get(element.buttonProceedCheckoutnoCarrinho).click();
    }

    adicionarNovoEndereço(){
        cy.get(element.addnewendereço).type('Endereço de Teste Nwe York');
        cy.screenshot();
    }

    clicarnobotãoProceedtoCheckoutNoAddress(){
        cy.get(element.buttonProceedCheckoutnoAddress).click();
    }

    clicarNoCheckAceitarTermos(){
        cy.get(element.checkaceitartermos).click();
        cy.screenshot();
    }

    clicarnobotãoProceedtoCheckoutNoShipping(){
        cy.get(element.buttonProceedCheckoutnoShipping).click();
    }

    selecionarCartãoComoFormaDePagamento(){
        cy.get(element.selectCartão).click();
        cy.screenshot();
    }

    selecionarChequeComoFormaDePagamento(){
        cy.get(element.selectCheque).click();
    }

    clicarNoBotãoDeConfirmarOrdem(){
        cy.get(element.buttonConfirmOrdem).click();
        cy.screenshot();
    }

    clicarnoBotãoDeVerificarOrdens(){
        cy.get(element.buttonverificarOrdens).click();
        cy.screenshot();    
    }
    
    verificarOrdem(){
        cy.get(element.verificarOrdem).click();
        cy.screenshot();
    }

    verificarDetalhesDaOrdem(){
        cy.route('GET', '**/index.php?controller=order-detail&id_order=**').as('postDetails');
        cy.get(element.verificarDetalhesDaOrdem).click();
        cy.wait('@postDetails').then((xhr) => {
            expect(xhr.status).be.eq(200);
        }) 
        cy.wait(500);
        cy.screenshot();
    }

    downloadpdf(){
        cy.get(element.buttondownload).click();
    }

}


export default new Compra();