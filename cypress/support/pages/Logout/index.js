const element = require('./elements').ELEMENTS;

class Logout{

clicarButtonLogout(){
    cy.get(element.ButtonLogout).click();
}  

validarLogout(){
    cy.get(element.ValidarLogout).contains("Authentication");
    cy.screenshot();
}

}
export default new Logout();