const element = require('./elements').ELEMENTS;

class Login{
    preencherEmail(){
        cy.get(element.Email).type("laercioteste400@hotmail.com");
    }

    preencherSenha(){
        cy.get(element.Password).type("12345");
    }

    clicarBotãoLogin(){
        cy.get(element.ButtonLogin).click();
    }

    validarLogin(){
        cy.get(element.ValidarLoginComSucesso).contains("Welcome to your account. Here you can manage all of your personal information and orders.")
        cy.screenshot();
    }


}

export default new Login();