

const element = require('./elements').ELEMENTS;

class Cadastro{
    preencheremailaddress(){

        cy.get(element.Emailaddress).type('laercioteste411@hotmail.com');

    }

    clicarbotãoCriarConta(){
        cy.route('POST', '**/index.php').as('postIndex.php')
        cy.get(element.ButtonCreateAccount).click();
    }

    validarAcessoaTelaDeCadastro(){
        cy.wait('@postIndex.php').then((xhr) => {
        expect(xhr.status).be.eq(200);
    })
    }

    selecionarTitle(){
        cy.get(element.TitleMr).click();
    }

    preencherFirstName(){
        cy.get(element.FirstName).type("Laércio");
    }

    preencherLastName(){
        cy.get(element.LastName).type("Gonçalves de Andrade");
    }

    preencherPassword(){
        cy.get(element.Password).type("12345")
    }

    selecionarDia(){
        cy.get(element.Day).select('21')
    }

    selecionarMes(){
        cy.get(element.Month).select('June')
    }

    selecionarAno(){
        cy.get(element.Years).select('1997')
    }

    clicarAssinatura(){
        cy.get(element.CheckboxAssinatura).click();
    }

    clicarReceberOfertas(){
        cy.get(element.CheckboxReceberOfertas).click();
    }

    preencherCompany(){
        cy.get(element.Company).type('Teste');
    }

    preencherEndereço(){
        cy.get(element.Address1).type('Nova York 4000');
    }

    preencherLugarEndereço(){
        cy.get(element.Address2).type('Prédio Alpha');
    }

    preencherCity(){
        cy.get(element.City).type('Nova York');
    }
    
   selecionarStates(){
       cy.get(element.States).select('New York');   
   }

    preencherCep(){
        cy.get(element.PostCode).type('48519');
    }

    preencherInformaçõesAdicionais(){
        cy.get(element.AdditionalInformation).type('Realizando Testes de Automação');
    }

    preencherTelefone(){
        cy.get(element.Phone).type('(11)1111-1111');
    }

    preencherTelefoneCelular(){
        cy.get(element.Mobile).type('3123124124');
    }

    preencheremailalternativo(){
        cy.get(element.EmailAlternative).type('My address').clear();
        cy.wait(500);
        cy.get(element.EmailAlternative).type('laercioteste100@gmail.com');
        cy.screenshot();
    }

    clicarBotãoRegistro(){
        cy.get(element.ButtonRegister).click();
    }

    validarmensagemCadastroComSucesso(){
        cy.get(element.MsgCadastroComSucesso).contains('Welcome to your account. Here you can manage all of your personal information and orders.');
        cy.screenshot();
    }
}

export default new Cadastro();