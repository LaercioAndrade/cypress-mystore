export const ELEMENTS = {

Emailaddress: '#email_create',
ButtonCreateAccount: '#SubmitCreate > span',
TitleMr: '#uniform-id_gender1',
FirstName: '#customer_firstname',
LastName: '#customer_lastname',
Password: '#passwd',
Day: '#days',
Month: '#months',
Years: '#years',
CheckboxAssinatura: '#newsletter',
CheckboxReceberOfertas: '#optin',
Company: '#company',
Address1: '#address1',
Address2: '#address2',
City: '#city',
States: '#id_state',
PostCode: '#postcode',
AdditionalInformation: '#other',
Phone: '#phone',
Mobile: '#phone_mobile',
EmailAlternative: '#alias',
ButtonRegister: '#submitAccount > span',
MsgCadastroComSucesso: '.info-account'

}

